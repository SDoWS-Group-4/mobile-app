package dtu.customer.models;


import java.io.Serializable;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;


public class Customer implements Serializable {
    
    // Init string to remove Serializable problems
    @Getter @Setter private String id = "";
    @Getter @Setter private String firstName = "";
    @Getter @Setter private String lastName = "";
    @Getter @Setter private String cprNumber = "";
    @Getter @Setter private String bankAccount = "";
    @Getter @Setter private ArrayList<String> tokens = new ArrayList<>(); 
    
    public Customer() {
    }

    public Customer(String firstName,String lastName,String cprNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cprNumber = cprNumber;
   
    }

    
    public Customer(String firstName,String lastName,String cprNumber,String bankAccount) {
         this.firstName = firstName;
         this.lastName = lastName;
         this.cprNumber = cprNumber;
         this.bankAccount = bankAccount;
      
    }

}
