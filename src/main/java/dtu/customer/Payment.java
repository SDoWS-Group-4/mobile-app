package dtu.customer;

import lombok.Getter;
import lombok.Setter;

public class Payment {

    @Getter @Setter private String id;
    @Getter @Setter private String token;
    @Getter @Setter private String sender;
    @Getter @Setter private String receiver;
    @Getter @Setter private Double amount;

    public Payment() {}

    @Override
    public String toString() {
    
        return "id:" + this.id + "token:" + this.token + "sender:" + this.sender + ", receiver:" + this.receiver + ", amount:" + this.amount;
    }

    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true 
        if (o == this) {
            return true;
        }
        Payment p = (Payment) o;
        return p.toString().equals(this.toString());
    }
}
