package dtu.customer.stores;

import java.util.ArrayList;
import java.util.List;

public class TokenStore {
    
    private List<String> tokens = new ArrayList<>();
    private static TokenStore single_instance = null;

    public List<String> addToken(String token){
        if (!tokens.contains(token)) {
            tokens.add(token);
        }
        return tokens;
    }

    public List<String> retireToken(String token){
        tokens.remove(token);
        return tokens;
    }

    public static TokenStore getInstance() {
        if (single_instance == null)
            single_instance = new TokenStore();

        return single_instance;
    }
}
