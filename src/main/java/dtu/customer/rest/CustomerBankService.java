package dtu.customer.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import dtu.commen.DTUPayManager;
import dtu.customer.models.Customer;


public class CustomerBankService {
    DTUPayManager manger;
    private int number = 0;
    ArrayList<Customer> usedCustomers = new ArrayList<>();
    
    public CustomerBankService(){
        this.manger = new DTUPayManager();
    }
    
    private void count(){
        this.number += 1 ;
    }
    private void decount(){
        this.number -= 1 ;
    }
    public Customer createCustomer(String balance){
        String bankAccount = "";
        Customer customer = new Customer(
            "Customer Jeff "+this.number,
            "Gated-Muskrat "+this.number,
            UUID.randomUUID().toString());
        try {
            bankAccount = manger.register(customer, balance);
        } catch (Exception e) {

        }
        customer.setBankAccount(bankAccount);
        count();
        usedCustomers.add(customer);
        return customer;
    }

    public void deleteCustomer(Customer customer){
        for (Customer c : usedCustomers) {
            try {
                manger.deregister(c.getBankAccount());
            } catch (Exception e) {
            }
            decount();
        }
       
    }

}