package dtu.customer.rest;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;


import dtu.commen.Payment;
import dtu.customer.models.Customer;

public class CustomerDTUPayService {

	private static String URL = "http://localhost:8080/customer";

	WebTarget baseUrl;
	Response response;
	Exception exception;

	public CustomerDTUPayService() {
		baseUrl = ClientBuilder.newClient().target(URL);
	}
	
	public String register(Customer customer) throws Exception {
		response =  baseUrl.path("register")
		.request()
		.post(Entity.json(customer));
		if(response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		String s = response.readEntity(String.class);
		response.close();
		return s;
	}
	
	public String deregister(String id) throws Exception {
		response = baseUrl
		.path("deregister")
		.queryParam("id", id)
		.request()
		.delete();
		if(response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		String s = response.readEntity(String.class);
		response.close();
		return s;
	}

	public ArrayList<String> getToken(String id, int amount) throws Exception {

		 Response response = baseUrl.path("token")
		.queryParam("id", id)
		.queryParam("amount", amount)
		.request()
		.get();

		if (response.getStatus() != 200){
			throw new Exception(response.readEntity(String.class));
		}
		ArrayList<String> arr = response.readEntity(new GenericType<ArrayList<String>>() {});
		response.close();
		return arr;
	}

	public List<Payment> getReport(String id) throws Exception {
		 Response r = baseUrl.path("report")
		.queryParam("id", id)
		.request()
		.get();
		if (r.getStatus() != 200){
			throw new Exception(r.readEntity(String.class));
		}
		return r.readEntity(new GenericType<ArrayList<Payment>>() {});
	}

}
