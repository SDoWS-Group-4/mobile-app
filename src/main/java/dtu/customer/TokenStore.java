package dtu.customer;


import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class TokenStore {
    
   @Getter private List<String> tokens = new ArrayList<>();
    

    public void addTokens(List<String> tokens){
        // tokens.forEach(t -> this.tokens.add(t));
        this.tokens.addAll(tokens);
    }

    public List<String> retireToken(String token){
        tokens.remove(token);
        return tokens;
    }

}
