package dtu.commen;

import java.util.ArrayList;
import java.util.HashMap;

import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import dtu.customer.models.Customer;
import dtu.merchant.models.Merchant;

public class DTUPayManager {

	private static String URL = "http://localhost:8082/bank";
	private static String URLManager = "http://localhost:8082/manager";
	private static String BANKURL = "http://fm-00.compute.dtu.dk:80";
	WebTarget managerBaseUrl;
	WebTarget baseUrl;
	WebTarget bankUrl;
	Response response;
	Exception exception;

	public DTUPayManager() {
		baseUrl = ClientBuilder.newClient().target(URL);
		managerBaseUrl = ClientBuilder.newClient().target(URLManager);
		// Only for testing purposes.
		bankUrl = ClientBuilder.newClient().target(BANKURL);
	}

	public String getBalance(String bankAccount) throws Exception {

		Response response = bankUrl
				.path("rest")
				.path("accounts")
				.path(bankAccount)
				.request()
				.get();
		if (response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		JsonObject jsonObject = response.readEntity(JsonObject.class);
		String s = jsonObject.get("balance").toString();
		int b = (int) Math.round(Double.parseDouble(s));
		return String.valueOf(b);
	}

	public String register(Customer customer, String balance) throws Exception {
		Response response = baseUrl.path("register")
				.queryParam("balance", balance)
				.request()
				.post(Entity.json(customer));
		if (response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		return response.readEntity(String.class);
	}

	public String register(Merchant Merchant, String balance) {
		Response response = baseUrl.path("register")
				.queryParam("balance", balance)
				.request()
				.post(Entity.json(Merchant));
		return response.readEntity(String.class);
	}

	public String deregister(String bankAccountId) throws Exception {
		Response response = baseUrl.path("deregister")
				.queryParam("id", bankAccountId)
				.request()
				.delete();
//		if (response.getStatus() != 200) {
//		}
		return response.readEntity(String.class);
	}

	public HashMap<String, ArrayList<Payment>> getReport() throws Exception {
		Response r = managerBaseUrl.path("report")
				.request()
				.get();
		if (r.getStatus() != 200){
			throw new Exception(r.readEntity(String.class));
		}
		return r.readEntity(new GenericType<HashMap<String, ArrayList<Payment>>>() {});
	}
}
