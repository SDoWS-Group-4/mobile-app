package dtu.merchant.rest;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import dtu.commen.Payment;
import dtu.merchant.models.Merchant;

import java.util.ArrayList;
import java.util.List;

public class MerchantDTUPayService {

	private static String URL = "http://localhost:8081/merchant";

	WebTarget baseUrl;
	Response response;

	public MerchantDTUPayService() {
		baseUrl = ClientBuilder.newClient().target(URL);
	}

	public String register(Merchant merchant) throws Exception {
		response = baseUrl.path("register")
				.request()
				.post(Entity.json(merchant));
		if (response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		return response.readEntity(String.class);
	}

	public String deregister(String id) throws Exception {
		response = baseUrl
				.path("deregister")
				.queryParam("id", id)
				.request()
				.delete();
		if (response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		return response.readEntity(String.class);
	}

	public void pay(Payment payment) throws Exception {
		response = baseUrl.path("pay")
				.request()
				.post(Entity.json(payment));
		if (response.getStatus() != 200) {
			throw new Exception(response.readEntity(String.class));
		}
		response.close();
	}

	public List<Payment> getReport(String id) throws Exception {
		Response r = baseUrl.path("report")
				.queryParam("id", id)
				.request()
				.get();
		if (r.getStatus() != 200){
			throw new Exception(r.readEntity(String.class));
		}
		return r.readEntity(new GenericType<ArrayList<Payment>>() {});
	}
}
