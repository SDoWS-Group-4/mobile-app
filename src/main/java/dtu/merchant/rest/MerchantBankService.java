package dtu.merchant.rest;

import java.util.ArrayList;
import java.util.UUID;

import dtu.commen.DTUPayManager;

import dtu.merchant.models.Merchant;

public class MerchantBankService {
    DTUPayManager manger;
    private int number = 0;
    ArrayList<Merchant> usedMerchants = new ArrayList<>();
    
    public MerchantBankService(){
        this.manger = new DTUPayManager();
    }
    
    private void count(){
        this.number += 1 ;
    }
    private void decount(){
        this.number -= 1 ;
    }
    public Merchant createMerchant(String balance){
        String bankAccount = "";
        Merchant merchant = new Merchant(
            "Merchant Jeff "+this.number,
            "Gated-Muskrat "+this.number,
            UUID.randomUUID().toString());
        try {
            bankAccount = manger.register(merchant, balance);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        merchant.setBankAccount(bankAccount);
        count();
        usedMerchants.add(merchant);
        return merchant;
    }

    public void deleteMerchant(Merchant merchant){
        for (Merchant m : usedMerchants) {
            try {
                manger.deregister(m.getBankAccount());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            decount();
        }
       
    }
}
