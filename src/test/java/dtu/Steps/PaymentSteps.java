package dtu.Steps;

import dtu.HelperClasses.Database;
import dtu.HelperClasses.Services;
import dtu.commen.DTUPayManager;
import dtu.commen.Payment;
import dtu.customer.models.Customer;
import dtu.customer.rest.CustomerBankService;
import dtu.customer.rest.CustomerDTUPayService;
import dtu.merchant.models.Merchant;
import dtu.merchant.rest.MerchantBankService;
import dtu.merchant.rest.MerchantDTUPayService;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PaymentSteps {
    CustomerDTUPayService customerDTUPayService = Services.getCPayInstance();
    MerchantDTUPayService merchantDTUPayService = Services.getMPayInstance();
    CustomerBankService customerBankService = Services.getCBankInstance();
    MerchantBankService merchantBankService = Services.getMBankInstance();
    DTUPayManager dtuPayManager = Services.getManagerInstance();

    Exception exception = null;
    Database db = Database.getInstance();
    Customer customer = null;
    Merchant merchant = null;

    @When("the merchant request a transfor of {string} kr")
    public void theMerchantRequestATransforOfKr(String amount) {
        customer = db.getCustomerInstance();
        merchant = db.getMerchantInstance();

        Payment payment = new Payment();

        payment.setSender(customer.getTokens().remove(0));
        payment.setReceiver(merchant.getId());
        payment.setAmount(Double.parseDouble(amount));
        try {
            merchantDTUPayService.pay(payment);
        } catch (Exception e) {
            exception = e;
        }

    }

    @Then("the money is succesfully transfered")
    public void theMoneyIsSuccesfullyTransfered() {
        assertNull(exception);
    }


    @Then("the balance of the customer is {string} kr")
    public void theBalanceOfTheCustomerIsKr(String expected) {
        // Write code here that turns the phrase above into concrete actions
        String balance = "";
        try {
            balance = dtuPayManager.getBalance(customer.getBankAccount());
        } catch (Exception e) {

        }
        assertEquals(balance, expected);
    }

    @Then("the balance of the merchant is {string} kr")
    public void theBalanceOfTheMerchantIsKr(String expected) {
        String balance = "";
        try {
            balance = dtuPayManager.getBalance(merchant.getBankAccount());
        } catch (Exception e) {
            System.out.println("Error getting balance" + balance);
        }
        assertEquals(expected, balance);
    }


    @And("the customer has {int} tokens")
    public void theCustomerHasTokens(int tokens) {
        customer = db.getCustomerInstance();
        System.out.println(customer.getTokens());
        assertEquals(tokens, customer.getTokens().size());
    }

    @Given("a customer with firstname {string} lastname {string} and CPR {string} and balance {string} kr")
    public void aCustomerWithFirstnameLastnameAndCPRAndBalanceKr(String firstname, String lastname, String CPR, String balance) throws Exception {
        Customer customer = new Customer(firstname, lastname, CPR);
        try {
            customer.setBankAccount(dtuPayManager.register(customer, balance));
            ;
        } catch (Exception e) {
            exception = e;
        }
    }

    @Given("a merchant with firstname {string} lastname {string} and CPR {string} and balance {string} kr")
    public void aMerchantWithFirstnameLastnameAndCPRAndBalanceKr(String firstname, String lastname, String CPR, String balance) {
        Merchant merchant = new Merchant(firstname, lastname, CPR);
        try {
            merchant.setBankAccount(dtuPayManager.register(merchant, balance));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("pay: an error message is returned saying {string}")
    public void payAnErrorMessageIsReturnedSaying(String string) {
        assertEquals(string, exception.getMessage());
    }

    @When("the merchant request a transfor of {string} kr with invalid token: {string}")
    public void theMerchantRequestATransforOfKrWithInvalidToken(String amount, String token) {
        merchant = db.getMerchantInstance();

        Payment payment = new Payment();
        payment.setSender(token);
        payment.setReceiver(merchant.getId());
        payment.setAmount(Double.parseDouble(amount));
        try {
            merchantDTUPayService.pay(payment);
        } catch (Exception e) {
            exception = e;
        }
    }

    @When("the merchant request a transfor of {string} kr with invalid receiver {string}")
    public void theMerchantRequestATransforOfKrWithInvalidReceiver(String amount, String receiver) {
        customer = db.getCustomerInstance();
        merchant = db.getMerchantInstance();

        Payment payment = new Payment();
        payment.setSender(customer.getTokens().remove(0));
        payment.setReceiver(receiver);
        payment.setAmount(Double.parseDouble(amount));
        try {
            merchantDTUPayService.pay(payment);
        } catch (Exception e) {
            exception = e;
        }
    }


    @After
    public void clean() throws Exception {
//        dtuPayManager.deregister(customer.getBankAccount());
//        dtuPayManager.deregister(merchant.getBankAccount());

        customerBankService.deleteCustomer(customer);

        try {
            //customerDTUPayService.deregister(customer.getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        merchantBankService.deleteMerchant(merchant);

        try {
            //merchantDTUPayService.deregister(merchant.getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        db.flushCustomers();
    }


    @When("the merchant request a transfor of {string} kr with same token twice")
    public void theMerchantRequestATransforOfKrWithSameTokenTwice(String amount) {
        customer = db.getCustomerInstance();
        merchant = db.getMerchantInstance();

        Payment payment = new Payment();
        Payment payment1 = new Payment();

        String temp_Token = customer.getTokens().remove(0);
        payment.setSender(temp_Token);       //same token on payment
        payment1.setSender(temp_Token);     //same token on payment

        payment.setReceiver(merchant.getId());
        payment1.setReceiver(merchant.getId());

        payment.setAmount(Double.parseDouble(amount));
        payment1.setAmount(Double.parseDouble(amount));

        try {
            merchantDTUPayService.pay(payment);
            merchantDTUPayService.pay(payment1);
        } catch (Exception e) {
            exception = e;
        }
    }
}
