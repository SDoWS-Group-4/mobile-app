package dtu.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import javax.ws.rs.core.Response;

import dtu.merchant.models.Merchant;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DTUPayMerchantSteps {

    // DTUPayMerchant dtuPay = new DTUPayMerchant();
    // Merchant merchant = new Merchant();

    // Response response;
    // Exception exception;

    // @Given("a merchant with name {string} and bank account {string}")
    // public void aMerchantWithNameAndBankAccount(String name, String bankAccount) {
    //     merchant.setName(name);
    //     merchant.setBankAccount(bankAccount);
        

    // }

    // @When("the merchant is registers at DTUPay")
    // public void theMerchantIsRegistersAtDTUPay() {
    //     response = dtuPay.register(merchant);
    // }

    // @Then("the merchant recieves the created account")
    // public void theMerchantRecievesTheCreatedAccount() {
    //     Merchant m = response.readEntity(Merchant.class);
    //     assertEquals(merchant.getName(), m.getName());
    //     assertEquals(merchant.getBankAccount(), m.getBankAccount());
    //     assertNotNull(m.getId()); 
        
    // }

    // @Given("a merchant which is registered at DTUPay")
    // public void aMerchantWhichIsRegisteredAtDTUPay() {
    //         aMerchantWithNameAndBankAccount("Jane Doe","1236663210");
    //         theMerchantIsRegistersAtDTUPay();
    //         theMerchantRecievesTheCreatedAccount();
    // }

    // @When("the merchant is deleted at DTUPay")
    // public void theMerchantIsDeletedAtDTUPay() {
    //     response = dtuPay.deregister(merchant.getId());
    // }

    // @Then("the merchant recives that the account is deleted")
    // public void theMerchantRecivesThatTheAccountIsDeleted() {

    //      Merchant m = response.readEntity(Merchant.class);
    //     // assertEquals(merchant.getName(), m.getName());
    //     // assertEquals(merchant.getBankAccount(), m.getBankAccount());
    //     // assertNotNull(m.getId()); 

    //     assertEquals("expected", "actual");
    // }

    // @Then("the merchant account does not excist in the app")
    // public void theMerchantAccountDoesNotExcist() {
    //     // Write code here that turns the phrase above into concrete actions
    //     throw new io.cucumber.java.PendingException();
    // }



//    DTUPayMerchant dtuPay = new DTUPayMerchant();
//
//    Account customerAccount;
//    Account merchantAccount;
//    Response response = null;
//    Response transferResponse = null;
//    Exception exception;
//    HashMap<String, Account> accountIdentifiers = new HashMap<>();
//
//
//    @Given("a customer with a bank account with balance {int}")
//    public void aCustomerWithABankAccountWithBalance(Integer balance) {
//        customerAccount = new Account("Customer", "Peder", "Frederiksen", "5463486789", (double) balance);
//
//        try {
//            response = dtuPay.createAccount(
//                customerAccount.getFirstName(),
//                customerAccount.getLastName(),
//                customerAccount.getCPR(),
//                customerAccount.getBalance());
//
//                customerAccount.setBankAccountID(response.readEntity(String.class));
//                accountIdentifiers.put(customerAccount.getCPR(), customerAccount);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @Given("that the customer is registered with DTU Pay")
//    public void thatTheCustomerIsRegisteredWithDTUPay(){
//
//        Account acc = accountIdentifiers.get(customerAccount.getCPR());
//        Response response = null;
//        try {
//            response = dtuPay.getAccountByAccountIdentifier(acc.getBankAccountID());
//            Map<String, String> json = response.readEntity(new GenericType<Map<String, String>>() {});
//            String accountIdentifier = (String) json.get("accountIdentifier");
//            assertEquals(200, response.getStatus());
//            assertEquals(acc.getBankAccountID(), accountIdentifier);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @Given("a merchant with a bank account with balance {int}")
//    public void aMerchantWithABankAccountWithBalance(Integer balance){
//        // // Write code here that turns the phrase above into concrete actions
//        merchantAccount = new Account("Merchant", "Jan", "Eriksen", "4468456889", (double) balance);
//
//        try {
//            response = dtuPay.createAccount(
//                merchantAccount.getFirstName(),
//                merchantAccount.getLastName(),
//                merchantAccount.getCPR(),
//                merchantAccount.getBalance());
//
//
//            merchantAccount.setBankAccountID(response.readEntity(String.class));
//            accountIdentifiers.put(merchantAccount.getCPR(), merchantAccount);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @Given("that the merchant is registered with DTU Pay")
//    public void thatTheMerchantIsRegisteredWithDTUPay() {
//
//        Account acc = accountIdentifiers.get(merchantAccount.getCPR());
//        Map<String, String> json = null;
//        Response response = null;
//
//        try {
//            response = dtuPay.getAccountByAccountIdentifier(acc.getBankAccountID());
//            assertEquals(200, response.getStatus());
//            json = response.readEntity(new GenericType<Map<String, String>>() {});
//            String accountIdentifier = (String) json.get("accountIdentifier");
//            assertEquals(acc.getBankAccountID(), accountIdentifier);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @When("the merchant initiates a payment for {int} kr by the customer")
//    public void theMerchantInitiatesAPaymentForKrByTheCustomer(Integer amount) {
//
//        try {
//        transferResponse = dtuPay.transferMoney(
//                customerAccount.getBankAccountID(),
//                merchantAccount.getBankAccountID(),
//                (double) amount,
//                "Transfer Money");
//        } catch (Exception e) {
//            exception = e;
//            e.printStackTrace();
//            exception = e;
//
//        }
//    }
//
//    @Then("the balance of the merchant at the bank is {int} kr")
//    public void theBalanceOfTheMerchantAtTheBankIsKr(Integer balance) {
//
//        try {
//            Response response = dtuPay.getAccountByAccountIdentifier(merchantAccount.getBankAccountID());
//            Map<String, String> json = response.readEntity(new GenericType<Map<String, String>>() {});
//            String balanceRecived = (String) json.get("balance");
//            DecimalFormat format = new DecimalFormat("0.0");
//            assertEquals(format.format(balance), balanceRecived);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @Then("the balance of the customer at the bank is {int} kr")
//    public void theBalanceOfTheCustomerAtTheBankIsKr(Integer balance) {
//
//        try {
//            Response response = dtuPay.getAccountByAccountIdentifier(customerAccount.getBankAccountID());
//            Map<String, String> json = response.readEntity(new GenericType<Map<String, String>>() {});
//
//            String balanceRecived = (String) json.get("balance");
//            DecimalFormat format = new DecimalFormat("0.0");
//
//            assertEquals(format.format(balance), balanceRecived);
//        } catch (Exception e) {
//            exception = e;
//        }
//    }
//
//    @Then("the payment is successful")
//    public void thePaymentIsSuccessful() {
//        assertEquals(201, transferResponse.getStatus());
//    }
//
//
//    @Then("the payment is unsuccessful")
//    public void thePaymentIsUnsuccessful() {
//        // Write code here that turns the phrase above into concrete actions
//        assertNotEquals(200, transferResponse.getStatus());
//    }
//
//    @Then("an error message is returned saying {string} is thrown")
//    public void anErrorMessageIsReturnedSayingIsThrown(String errormsg) {
//        // Write code here that turns the phrase above into concrete actions
//        System.out.println("" + transferResponse.getStatus());
//        System.out.println(exception.getMessage());
//        assertEquals(errormsg, exception.getMessage());
//
//    }
//
//    @After
//    public void cleanup() {
//        accountIdentifiers.forEach((k, account) -> {
//            try {
//                dtuPay.deleteAccount(account.getBankAccountID());
//            } catch (Exception e) {
//
//                e.printStackTrace();
//            }
//        });
//
//    }


}
