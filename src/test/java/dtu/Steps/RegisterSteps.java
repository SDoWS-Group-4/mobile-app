package dtu.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import dtu.HelperClasses.Database;
import dtu.HelperClasses.Services;
import dtu.commen.DTUPayManager;
import dtu.customer.models.Customer;
import dtu.customer.rest.CustomerBankService;
import dtu.customer.rest.CustomerDTUPayService;
import dtu.merchant.models.Merchant;
import dtu.merchant.rest.MerchantDTUPayService;
import dtu.merchant.rest.MerchantBankService;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegisterSteps {
    Customer customer = null;
    Merchant merchant = null;
    CustomerDTUPayService customerDTUPayService = Services.getCPayInstance();
    MerchantDTUPayService merchantDTUPayService = Services.getMPayInstance();
    CustomerBankService customerBankService =  Services.getCBankInstance();
    MerchantBankService merchantBankService = Services.getMBankInstance();
    DTUPayManager dtuPayManager = Services.getManagerInstance();
    String CustomerBalance;
    String MerchantBalance;
    Exception exception = null;
    Database db = Database.getInstance();

    @Given("a customer with balance {string} kr")
    public void aCustomerWithFirstnameLastnameAndCPRAndBalanceKr(String balance) {
        this.customer = customerBankService.createCustomer(balance);
        db.addCustomer(customer);

    }

    @Given("a merchant with balance {string} kr")
    public void aMerchantWithBalanceKr(String balance) {
        // Write code here that turns the phrase above into concrete actions
        this.merchant = merchantBankService.createMerchant(balance);
        db.addMerchant(merchant);
    }

    @When("the customer registers at DTUPay")
    public void theCustomerIsRegistersAtDTUPay() {
        try {
            customer.setId(customerDTUPayService.register(customer));
        } catch (Exception e) {
            exception = e;
        }

    }

    @When("the merchant registers at DTUPay")
    public void theMerchantRegistersAtDTUPay() {
        // Write code here that turns the phrase above into concrete actions
        try {
            merchant.setId(merchantDTUPayService.register(merchant));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Given("the customer is registered at DTUPay")
    public void theCustomerIsRegisteredAtDTUPay() {
        assertNotNull(customer.getId());
    }

    @Then("the merchant is registered at DTUPay")
    public void theMerchantIsRegisteredAtDTUPay() {
        // Write code here that turns the phrase above into concrete actions
        assertNotNull(merchant.getId());
    }

    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String string) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(string, exception.getMessage());
    }

    @After
    public void clean() {

        customerBankService.deleteCustomer(customer);

        try {
            customerDTUPayService.deregister(customer.getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        merchantBankService.deleteMerchant(merchant);

        try {
            merchantDTUPayService.deregister(merchant.getId());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        db.flushCustomers();
    }




    // public void removeMerchants(){
    // merchantBankService.deleteMerchant(merchant);
    // }

    // }
    


}
