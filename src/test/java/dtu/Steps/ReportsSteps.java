package dtu.Steps;

import dtu.HelperClasses.Services;
import dtu.commen.DTUPayManager;
import dtu.commen.Payment;
import dtu.customer.models.Customer;
import dtu.customer.rest.CustomerDTUPayService;
import dtu.merchant.models.Merchant;
import dtu.merchant.rest.MerchantDTUPayService;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.de.Aber;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ReportsSteps {
    DTUPayManager dtuPayManager = Services.getManagerInstance();
    CustomerDTUPayService customerDTUPayService = Services.getCPayInstance();
    MerchantDTUPayService merchantDTUPayService = Services.getMPayInstance();
    Customer customer;
    Merchant merchant;
    Exception exception;
    ArrayList<Payment> report = new ArrayList<>();
    ArrayList<Payment> merchantReport = new ArrayList<>();
    HashMap<String, ArrayList<Payment>> managerReport = new HashMap<>();


    @Given("an existing customer with firstname {string} lastname {string} and CPR {string} and balance {string} kr")
    public void anExistingCustomerWithFirstnameLastnameAndCPRAndBalanceKr(String firstName, String lastName, String CPR, String balance) {
        customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(CPR);
        try {
            customer.setBankAccount(dtuPayManager.register(customer, balance));
            customer.setId(customerDTUPayService.register(customer));
            customer.setTokens(customerDTUPayService.getToken(customer.getId(), 5));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("an existing merchant with firstname {string} lastname {string} and CPR {string} and balance {string} kr")
    public void anExistingMerchantWithFirstnameLastnameAndCPRAndBalanceKr(String firstName, String lastName, String CPR, String balance) {
        merchant = new Merchant();
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(CPR);
        try {
            merchant.setBankAccount(dtuPayManager.register(merchant, balance));
            merchant.setId(merchantDTUPayService.register(merchant));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("the merchant request a transfer of {string} kr")
    public void theMerchantRequestATransferOfKr(String amount) {
        Payment payment = new Payment();

        payment.setSender(customer.getTokens().remove(0));
        payment.setReceiver(merchant.getId());
        payment.setAmount(Double.parseDouble(amount));
        try {
            merchantDTUPayService.pay(payment);
        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
        }
    }

    @Then("the money is successfully transferred")
    public void theMoneyIsSuccessfullyTransferred() {
        assertNull(exception);
    }


    @When("the customer requests a report of payments")
    public void theCustomerRequestsAReportOfPayments() {
        try {
            report.addAll(customerDTUPayService.getReport(customer.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("the report contains the transaction with the merchant")
    public void theReportsContainsTheTransactionWithTheMerchant() {
        assertTrue(report.stream().anyMatch(p -> p.getReceiver().equals(merchant.getId())));
    }

    @When("the merchant requests a report of payments")
    public void theMerchantRequestsAReportOfPayments() {
        try {
            merchantReport.addAll(merchantDTUPayService.getReport(merchant.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("the report contains the transactions")
    public void theReportContainsTheTransactions() {
        assertTrue(merchantReport.stream().anyMatch(p -> p.getReceiver().equals(merchant.getId())));
    }

    @When("a manager requests a list of all payments")
    public void aManagerRequestsAListOfAllPayments() {
        try {
            managerReport = dtuPayManager.getReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("he gets a list of payments")
    public void heGetsAListOfPayments() {
        assertFalse(managerReport.isEmpty());
    }

    @After("@test")
    public  void tearDown() {
        try {
            dtuPayManager.deregister(customer.getBankAccount());
            customerDTUPayService.deregister(customer.getId());

            dtuPayManager.deregister(merchant.getBankAccount());
            merchantDTUPayService.deregister(merchant.getId());
        } catch (Exception e) {
            System.out.println("Error in teardown!");
            e.printStackTrace();
        }
    }
}
