package dtu.Steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import dtu.HelperClasses.Database;
import dtu.HelperClasses.Services;
import dtu.commen.DTUPayManager;
import dtu.customer.models.Customer;
import dtu.customer.rest.CustomerBankService;
import dtu.customer.rest.CustomerDTUPayService;
import dtu.merchant.models.Merchant;
import dtu.merchant.rest.MerchantBankService;
import dtu.merchant.rest.MerchantDTUPayService;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class TokenSteps {
    CustomerDTUPayService customerDTUPayService = Services.getCPayInstance();
    MerchantDTUPayService merchantDTUPayService = Services.getMPayInstance();
    CustomerBankService customerBankService =  Services.getCBankInstance();
    MerchantBankService merchantBankService = Services.getMBankInstance();

    Database db = Database.getInstance();
    Exception exception = null;
    Customer customer = null;
    Merchant merchant = null;

    @When("the customer request {int} tokens")
    public void theCustomerRequestTokens(Integer tokensAmount) {
        customer = db.getCustomerInstance();
        ArrayList<String> tokens = new ArrayList<>(); 
        
        try {
           tokens =  customerDTUPayService.getToken(customer.getId(), tokensAmount);
        } catch (Exception e) {
            exception = e;
        }
        if (!tokens.isEmpty()){
            customer.setTokens(tokens);
        }
    }

    @Then("Token: an error message is returned saying {string}")
    public void tokenAnErrorMessageIsReturnedSaying(String string) {
        Assert.assertEquals(string, exception.getMessage());
    }

    @Then("the customer recives {int} tokens")
    public void theCustomerRecivesTokens(Integer tokens) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(tokens,customer.getTokens().size());
    }

    // @Given("a customer which is registered at DTUPay")
    // public void aCustomerWhichIsRegisteredAtDTUPay() {
    // // Write code here that turns the phrase above into concrete actions
    // throw new io.cucumber.java.PendingException();
    // }

    // @Given("the customer has {int} tokens")
    // public void theCustomerHasTokens(Integer int1) {
    // // Write code here that turns the phrase above into concrete actions
    // throw new io.cucumber.java.PendingException();
    // }

    // @When("the customer request {int} tokens")
    // public void theCustomerRequestTokens(Integer int1) {
    // // Write code here that turns the phrase above into concrete actions
    // throw new io.cucumber.java.PendingException();
    // }

    // @Then("the customer recives {int} tokens")
    // public void theCustomerRecivesTokens(Integer int1) {
    // // Write code here that turns the phrase above into concrete actions
    // throw new io.cucumber.java.PendingException();
    // }

    //
    // @When("the customer request {int} tokens")
    // public void theCustomerRequestTokens(Integer tokenAmount) {
    // try {
    // tokens = dtuPay.getToken(customer.getId(), tokenAmount);
    // customer.getTokenStore().addTokens(tokens);
    // } catch (Exception e) {
    // exception = e;
    // }
    // }
    //
    // @Then("the customer recives {int} tokens")
    // public void theCustomerRecivesNewTokens(Integer tokenAmount) {
    // assertEquals(tokenAmount, customer.getTokenStore().getTokens());
    // }
    //
    // @When("the customer request tokens again")
    // public void theCustomerRequestTokensAgain() {
    // theCustomerRequestTokens(5);
    // }

    @After
    public void clean() {

        customerBankService.deleteCustomer(customer);

        try {
            customerDTUPayService.deregister(customer.getId());
        } catch (Exception e) {
        }
        merchantBankService.deleteMerchant(merchant);
        try {
            merchantDTUPayService.deregister(merchant.getId());
        } catch (Exception e) {
        }
        db.flushCustomers();
    }

}
