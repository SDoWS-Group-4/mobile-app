package dtu.HelperClasses;

import java.util.ArrayList;
import java.util.List;

import dtu.commen.Payment;
import dtu.customer.models.Customer;
import dtu.customer.rest.CustomerBankService;
import dtu.merchant.models.Merchant;
import lombok.Getter;
import lombok.Setter;
// ONLY FOR TEST !
public class Database {
    // Static variable reference of single_instance
    // of type Singleton

  
    @Getter @Setter private List<Customer> customers = new ArrayList<>();
    @Getter @Setter private List<Merchant> merchants = new ArrayList<>();
    @Getter @Setter private List<Payment> payments = new ArrayList<>();
    @Getter @Setter private Customer customerInstance = null;
    @Getter @Setter private Merchant merchantInstance = null;
    private static Database single_instance = null;
    // Static method
    // Static method to create instance of Singleton class

    private Database() {}
    
    public static Database getInstance() {
        if (single_instance == null)
            single_instance = new Database();

        return single_instance;
    }
    public Customer getCustomerInstance(){
        return this.customerInstance;
    }
    public Merchant getMerchantInstance(){
        return this.merchantInstance;
    }

    public Customer getCustomer(String CPR){
        return customers.stream().filter( c -> c.getCprNumber().equals(CPR))
            .findFirst()
            .get();
    }

    public Merchant getMerchant(String CPR){
        return merchants.stream().filter( c -> c.getCprNumber().equals(CPR))
            .findFirst()
            .get();
    }
    
    public void addCustomer(Customer customer){
        this.customerInstance = customer;
        customers.add(customer);
    }

    public void addMerchant(Merchant merchant){
        this.merchantInstance = merchant;
        merchants.add(merchant);
    }

    public void flushMerchants(){
        merchants.clear();
    }
    public void flushCustomers(){
        customers.clear();
    }

   

}