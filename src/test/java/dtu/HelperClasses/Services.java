package dtu.HelperClasses;

import dtu.commen.DTUPayManager;
import dtu.customer.rest.CustomerBankService;
import dtu.customer.rest.CustomerDTUPayService;
import dtu.merchant.rest.MerchantBankService;
import dtu.merchant.rest.MerchantDTUPayService;

public class Services {

    private static CustomerDTUPayService customerDTUPayServiceInstance = null;
    private static MerchantDTUPayService merchantDTUPayService = null;
    private static CustomerBankService customerBankService = null;
    private static MerchantBankService merchantBankService = null;
    private static DTUPayManager dtuPayManager = null;

    public static CustomerDTUPayService getCPayInstance() {
        if (customerDTUPayServiceInstance == null)
            customerDTUPayServiceInstance = new CustomerDTUPayService();
        return customerDTUPayServiceInstance;
    }

    public static MerchantDTUPayService getMPayInstance() {
        if (merchantDTUPayService == null)
        merchantDTUPayService = new MerchantDTUPayService();
        return merchantDTUPayService;
    }

    public static CustomerBankService getCBankInstance() {
        if (customerBankService == null)
        customerBankService = new CustomerBankService();
        return customerBankService;
    }
    public static MerchantBankService getMBankInstance() {
        if (merchantBankService == null)
        merchantBankService = new MerchantBankService();
        return merchantBankService;
    }
    public static DTUPayManager getManagerInstance() {
        if (dtuPayManager == null)
        dtuPayManager = new DTUPayManager();
        return dtuPayManager;
    }

}
