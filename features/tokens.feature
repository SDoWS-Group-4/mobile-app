Feature: tokens

    Scenario: Request token
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        Then the customer is registered at DTUPay
        Given the customer request 5 tokens
        Then the customer recives 5 tokens

     Scenario: Reqest token when less than two tokens
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         Then the customer is registered at DTUPay
         And the customer has 0 tokens
         When the customer request 1 tokens
         Then the customer has 1 tokens
         When the customer request 1 tokens
         Then the customer has 2 tokens

     Scenario: Request in total six tokens
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         Then the customer is registered at DTUPay
         And the customer has 0 tokens
         When the customer request 1 tokens
         Then the customer has 1 tokens
         When the customer request 5 tokens
         And the customer has 6 tokens

     Scenario: Reqest token when holding more than one tokens
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         Then the customer is registered at DTUPay
         And the customer has 0 tokens
         When the customer request 2 tokens
         Then the customer has 2 tokens
         When the customer request 1 tokens
        When the customer request 1 tokens
       Then the customer has 2 tokens
       And Token: an error message is returned saying "Customer has too many tokens"

     Scenario: Reqest more than five tokens
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         Then the customer is registered at DTUPay
         And the customer has 0 tokens
         When the customer request 6 tokens
         Then Token: an error message is returned saying "Please request 1-5 tokens"
         And the customer has 0 tokens

   Scenario: Request negative amount of tokens
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        Then the customer is registered at DTUPay
        When the customer request -1 tokens
        Then Token: an error message is returned saying "Please request an amount of tokens greater than zero"
        And the customer has 0 tokens
