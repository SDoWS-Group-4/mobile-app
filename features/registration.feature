Feature: Registration

    Scenario: Customer is registed at DTUpay
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        Then the customer is registered at DTUPay

    Scenario: Merchant is registed at DTUpay
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        Then the merchant is registered at DTUPay

    Scenario: Customer is already registed at DTUpay
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        Then the customer is registered at DTUPay
        Given  the customer registers at DTUPay
        When the customer registers at DTUPay
        Then an error message is returned saying "The customer already exists"

    Scenario: Register merchant
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        Then the merchant is registered at DTUPay
        When the merchant registers at DTUPay
        Then the merchant is registered at DTUPay
        Then an error message is returned saying "The merchant already exists"