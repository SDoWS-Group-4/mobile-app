@Token
Feature: Payment
     Scenario: Transfer money succesfully
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         When the customer request 1 tokens
         Then the customer has 1 tokens
         Given a merchant with balance "100" kr
         When the merchant registers at DTUPay
         When the merchant request a transfor of "10" kr
         Then the money is succesfully transfered
         And the balance of the merchant is "110" kr
         And the balance of the customer is "990" kr
         And the customer has 0 tokens

     Scenario: Transfer money unsuccesfully due to insufficient balance
         Given a customer with balance "1000" kr
         When the customer registers at DTUPay
         When the customer request 1 tokens
         Then the customer has 1 tokens
         Given a merchant with balance "100" kr
         When the merchant registers at DTUPay
         When the merchant request a transfor of "1001" kr
         Then pay: an error message is returned saying "Debtor balance will be negative"
         And the customer has 0 tokens

    Scenario: Transfer money unsuccesfully due to negative amount
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        When the customer request 1 tokens
        Then the customer has 1 tokens
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        When the merchant request a transfor of "-100" kr
        Then pay: an error message is returned saying "Amount must be positive"
        And the customer has 0 tokens

    Scenario: Transfer money with invalid sender
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        When the merchant request a transfor of "-100" kr with invalid token: "Fake customer"
        Then pay: an error message is returned saying "Account is not valid"

    Scenario: Transfer money with invalid receiver
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        When the customer request 1 tokens
        Then the customer has 1 tokens
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        When the merchant request a transfor of "-100" kr with invalid receiver "Fake merchant"
        Then pay: an error message is returned saying "The merchant with id: Fake merchant does not exists"

    Scenario: Transfer money with same token twice
        Given a customer with balance "1000" kr
        When the customer registers at DTUPay
        When the customer request 1 tokens
        Then the customer has 1 tokens
        Given a merchant with balance "100" kr
        When the merchant registers at DTUPay
        When the merchant request a transfor of "10" kr with same token twice
        Then pay: an error message is returned saying "Account is not valid"
        And the customer has 0 tokens
        And the balance of the merchant is "110" kr
        And the balance of the customer is "990" kr