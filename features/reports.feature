
@test
Feature: reports
     Scenario: Receive recording of payments for customer
         Given an existing customer with firstname "Monty" lastname "Python" and CPR "83942" and balance "1000" kr
         Given an existing merchant with firstname "Mr." lastname "Bean" and CPR "76283432" and balance "100" kr
         When the merchant request a transfer of "10" kr
         Then the money is successfully transferred
         When the customer requests a report of payments
         Then the report contains the transaction with the merchant
         When the merchant requests a report of payments
         Then the report contains the transactions
    
     Scenario: Receive list of all payments
         When a manager requests a list of all payments
         Then he gets a list of payments
#         Given a customer with firstname "Marc" lastname "Zucc" and CPR "123457890" and balance "1000" kr
#         And the customer is registered at DTUPay
#         Given a merchant with firstname "Bill" lastname "Gates" and CPR "123457891" and balance "100" kr
#         And the merchant is registered at DTUPay
#         When the merchant request a transfor of "10" kr
#         Then the money is succesfully transfered 
#         And the balance of the merchant is "110" kr
#         When the merchant requsts a recording of payments
#         Then the merchant receives a recording
#         And the record contains the transaction with the customers token

#     Scenario: Receive empty recording for merchant 
#         Given a merchant with firstname "Bill" lastname "Gates" and CPR "123457891" and balance "100" kr
#         And the merchant is registered at DTUPay
#         And the record at DTUPay of the merchant is empty
#         When the merchant requsts a recording of payments
#         Then the merchant receives a recording
#         And the record is empty

#     #no transactions

